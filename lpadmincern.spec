%define use_alternatives 1

%define debug_package %{nil}

Summary: CERN LDAP printer database client and integration sctipts
Name: lpadmincern
Version: 1.4.0
Release: 1%{?dist}
Source0: %{name}-%{version}.tgz
Group: CERN/Utilities
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Vendor:  CERN
License: GPL
Requires: cups >= 1.1
# Just until the manpage generation is moved out of the .pl file ...
BuildRequires: perl
BuildArch: noarch
Provides: /usr/bin/cancel /usr/bin/lp /usr/bin/lpq /usr/bin/lpr /usr/bin/lprm /usr/bin/lpstat /usr/sbin/lpc
Requires: /usr/sbin/alternatives
# below: to migrate on SLC4
Obsoletes: xprint-cups
Provides: xprint-cups
Obsoletes: cern-config-printers
Provides: cern-config-printers

#
# Canon foomatic-rip cups filter
#
Requires: sicgsfilter >= 2.0.6
#
# Canon URF II cups filter
#
# Requires: pstoufr2cpca >= 3.50

Requires: foomatic
Requires: python3
%if 0%{?rhel} == 7
Requires: foomatic-filters
Requires: perl, perl-LDAP
%endif

%if 0%{?rhel} >= 8
Requires: cups-filters, python3-ldap
Requires: systemd
BuildRequires: python3-devel
BuildRequires: systemd
Recommends: cron, logrotate
%endif


%description
lpadmincern is the CERN LDAP printer database client.
It acts as an frontend to system printing spooler - CUPS
and allows configuration of CERN printers on the system.

Additional scripts provide integration of local system
lp clients and CERN printing infrastructure.

%changelog
* Tue May 10 2022 Ben Morrice <ben.morrice@cern.ch> 1.4.0-1
- port lpadmincern from perl to python

* Wed Nov 17 2021 Daniel Juarez <djuarezg@cern.ch> 1.3.26-1
- Adapt lpq to Python3

* Mon Jun 14 2021 Steve Traylen <steve.traylen@cern.ch> 1.3.25-1
- Fix timer

* Wed Dec 11 2019 Steve Traylen <steve.traylen@cern.ch> 1.3.24-1
- Migrate cron to timer on 8 and newer

* Mon Nov 18 2019 Ben Morrice <ben.morrice@cern.ch> 1.3.23-1
- add sub routine to optimise 'update' calls

* Wed Nov 13 2019 Ben Morrice <ben.morrice@cern.ch> 1.3.22-3
- update spec for el8

* Thu Mar 21 2019 Thomas Oulevey <thomas.oulevey@cern.ch> 1.3.22-1
- add Canon iR ADV 4535
- add Canon iR ADV 525

* Thu May 03 2018 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.19-1
- add Canon iR ADV 5535I
- add Canon iR ADV 356I

* Wed Nov 02 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.18-1
- add Canon iR ADV 5535

* Thu Oct 13 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.17-3
- add reqs on foomatic(-filters) to make sure foomatic-rip is
  installed
* Wed May 04 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.17-2
- fixed logrotate conf permissions.

* Mon Feb 22 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.17
- add Canon iR ADV 400

* Wed Jan 20 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.16
- add HP LaserJet M506

* Fri Sep 18 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.15
- add HP Color laserJet M553 series.

* Wed Sep 02 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.14
- add HP LaserJet M606 series.

* Wed Sep 10 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.13
- use systemctl (for real this time ..)

* Mon Jul 14 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.12
- use systemctl if available

* Thu May 15 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.11
- added ppd for Canon Ir-adv 4245

* Fri Feb 28 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.9
- added missing HP printers ppds.
- default paper format changed from Letter to A4

* Tue Feb 25 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.8
- added multiple buildings option (patch from Mike Grozak)
- added missing ppds for 8 printer models (7 HP, 1 Canon)

* Fri Feb 21 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.7
- added dependency on Canon foomatic-rip filter 

* Thu Feb 20 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.6-2
- replaced Canon ppds for ones from CQue 2.0.6 driver 
  (allowing for 'SecurePrint' on Linux)
* Thu Feb 06 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3.4-1
- label cron.daily lpadmincern a config file.

* Tue Jul 30 2013 Thomas Oulevey <Thomas.Oulevey@cern.ch> 1.3.2-2
- adding HP LASERJET 700 M712

* Thu Feb 21 2013 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.3-1
- adding Canon iR-ADV - C5235i, 4025i, 4045i

* Wed Oct 05 2011 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.2.0
- cron configuration options now in /etc/sysconfig/lpadmincern

* Fri Mar 25 2011 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 1.1.9-1
- added ppd for Canon IR C5030

* Tue Feb 08 2011 Jan van Eldik <Jan.van.Eldik@cern.ch> 1.1.8-1
- added ppd for Canon IR3245

* Tue Nov 09 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.1.6-1
- added ppd for Canon LBP6650

* Mon Jul 12 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.1.3-3
- fixed duplex setup for HP LaserJet 4300 models.

* Thu Jun 03 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.1.3-2
- added trigger script for xprint-cups uninstall woes...

* Wed Mar 10 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.0.1
- cleanup of unused PPDs, better name/model bindings.
* Fri Mar 05 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.0
- Unify printer bindings on SLC4/SLC5
* Thu Mar 04 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0.9
- port old xprint-cups integration scripts
* Tue Mar 02 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0.7
- make it work for both SLC4 and SLC5
* Wed Feb 10 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0.6
- removed 1000 matches limitation.
* Wed May 27 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0.5
- fixed to use xldap.cern.ch
* Tue Jan 13 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0.4
- fixed the --default switch 
* Fri Oct 17 2008 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0.3
- fixes in some broken HP ppds
- fixes for Duplex enabled while Duplex Unit not installed cases..
  (on HP printers..)

* Fri Oct 17 2008 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0.1
- initial build 
%prep
%setup 
%if 0%{?rhel} >= 8
# note this will get changed by /usr/lib/rpm/redhat/brp-mangle-shebangs to /usr/libexec/platform-python
pathfix.py -pni "/usr/bin/python3 -W ignore::DeprecationWarning" l*-cern.py
%endif

%build

pod2man %{name}.pl > %{name}.1

%install
mkdir -p $RPM_BUILD_ROOT/%{_sbindir}
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1
%if 0%{?rhel} <= 7
mkdir -p $RPM_BUILD_ROOT/etc/cron.daily/
mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d/
%else
mkdir -p $RPM_BUILD_ROOT/%{_unitdir}
%endif
mkdir -p $RPM_BUILD_ROOT/etc/sysconfig/
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/%{name}/ppds/

%if 0%{?rhel} == 7
install -m 755 %{name}.pl $RPM_BUILD_ROOT/%{_sbindir}/%{name}
%else
# Python version used everywhere except 7
install -m 755 %{name}.py $RPM_BUILD_ROOT/%{_sbindir}/%{name}
%endif

install -m 644 %{name}.1 $RPM_BUILD_ROOT/%{_mandir}/man1/
install -m 644 ppds/* $RPM_BUILD_ROOT/%{_datadir}/%{name}/ppds/

install -m755 lpq-cern.py $RPM_BUILD_ROOT%{_bindir}/lpq.cern
install -m755 lprm-cern.py $RPM_BUILD_ROOT%{_bindir}/lprm.cern
%if 0%{?rhel} <= 7
install -m755 lpadmincern.cron $RPM_BUILD_ROOT/etc/cron.daily/lpadmincern
install -m644 lpadmincern.logrotate $RPM_BUILD_ROOT/etc/logrotate.d/lpadmincern
%else
install -p -m644 lpadmincern.service $RPM_BUILD_ROOT/%{_unitdir}/lpadmincern.service
install -p -m644 lpadmincern.timer $RPM_BUILD_ROOT/%{_unitdir}/lpadmincern.timer
%endif
install -m644 lpadmincern.sysconfig $RPM_BUILD_ROOT/etc/sysconfig/lpadmincern

pushd $RPM_BUILD_ROOT%{_bindir}
ln -sf cancel.cups cancel.cern
ln -sf lp.cups lp.cern
ln -sf lpr.cups lpr.cern
ln -sf lpstat.cups lpstat.cern
cd $RPM_BUILD_ROOT%{_sbindir}
ln -sf lpc.cups lpc.cern
popd

mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man8

install -m644 man/cancel-cern.1 $RPM_BUILD_ROOT%{_mandir}/man1/cancel-cern.1
install -m644 man/lp-cern.1 $RPM_BUILD_ROOT%{_mandir}/man1/lp-cern.1
install -m644 man/lpq-cern.1 $RPM_BUILD_ROOT%{_mandir}/man1/lpq-cern.1
install -m644 man/lpr-cern.1 $RPM_BUILD_ROOT%{_mandir}/man1/lpr-cern.1
install -m644 man/lprm-cern.1 $RPM_BUILD_ROOT%{_mandir}/man1/lprm-cern.1
install -m644 man/lpstat-cern.1 $RPM_BUILD_ROOT%{_mandir}/man1/lpstat-cern.1
install -m644 man/lpc-cern.8 $RPM_BUILD_ROOT%{_mandir}/man8/lpc-cern.8

pushd $RPM_BUILD_ROOT%{_mandir}/man1
gzip cancel-cern.1 lp-cern.1 lpq-cern.1 lpr-cern.1 lprm-cern.1 lpstat-cern.1
cd $RPM_BUILD_ROOT%{_mandir}/man8
gzip lpc-cern.8
popd

%clean

rm -rf $RPM_BUILD_ROOT



%files
%defattr(-,root,root)
%doc README
%{_sbindir}/%{name}
%{_bindir}/cancel.cern
%{_bindir}/lp.cern
%{_bindir}/lpq.cern
%{_bindir}/lpr.cern
%{_bindir}/lprm.cern
%{_bindir}/lpstat.cern
%{_sbindir}/lpc.cern
%dir %{_datadir}/%{name}/ppds
%{_datadir}/%{name}/ppds/*
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/cancel-cern.1.gz
%{_mandir}/man1/lp-cern.1.gz
%{_mandir}/man1/lpq-cern.1.gz
%{_mandir}/man1/lpr-cern.1.gz
%{_mandir}/man1/lprm-cern.1.gz
%{_mandir}/man1/lpstat-cern.1.gz
%{_mandir}/man8/lpc-cern.8.gz
%if 0%{?rhel} <= 7
/etc/logrotate.d/lpadmincern
%config(noreplace) /etc/cron.daily/lpadmincern
%else
%{_unitdir}/lpadmincern.service
%{_unitdir}/lpadmincern.timer
%endif
%config(noreplace) /etc/sysconfig/lpadmincern

%post

/usr/sbin/alternatives --install %{_bindir}/lpr print %{_bindir}/lpr.cern 55 \
	--slave %{_bindir}/cancel print-cancel %{_bindir}/cancel.cern \
         --slave %{_bindir}/lp print-lp %{_bindir}/lp.cern \
         --slave %{_bindir}/lpq print-lpq %{_bindir}/lpq.cern \
         --slave %{_bindir}/lprm print-lprm %{_bindir}/lprm.cern \
         --slave %{_bindir}/lpstat print-lpstat %{_bindir}/lpstat.cern \
         --slave %{_sbindir}/lpc print-lpc %{_sbindir}/lpc.cern \
         --slave %{_mandir}/man1/cancel.1.gz print-cancelman %{_mandir}/man1/cancel-cern.1.gz \
         --slave %{_mandir}/man1/lp.1.gz print-lpman %{_mandir}/man1/lp-cern.1.gz \
         --slave %{_mandir}/man1/lpq.1.gz print-lpqman %{_mandir}/man1/lpq-cern.1.gz \
         --slave %{_mandir}/man1/lpr.1.gz print-lprman %{_mandir}/man1/lpr-cern.1.gz \
         --slave %{_mandir}/man1/lprm.1.gz print-lprmman %{_mandir}/man1/lprm-cern.1.gz \
         --slave %{_mandir}/man1/lpstat.1.gz print-lpstatman %{_mandir}/man1/lpstat-cern.1.gz \
         --slave %{_mandir}/man8/lpc.8.gz print-lpcman %{_mandir}/man8/lpc-cern.8.gz \
         --initscript cups

/usr/sbin/alternatives --auto print

echo "enabling and starting cups, please wait..."
if [ -x /bin/systemctl ]; then
 /bin/systemctl enable cups 2>&1 >> /dev/null ||:
 /bin/systemctl start cups 2>&1 >> /dev/null ||:
else 
 /sbin/chkconfig --add cups 2>&1 >> /dev/null ||:
 /sbin/service cups restart 2>&1 >> /dev/null ||:
fi

echo "Updating all printers definitions, please wait..."
/usr/sbin/lpadmincern -u

%if 0%{?rhel} > 7
if [ $1 -eq 1 ] ; then
        # Initial installation
        systemctl --no-reload preset lpadmincern.timer &>/dev/null || :
fi
systemctl start lpadmincern.timer
%else
echo "Updating all printers definitions, please wait..."
/usr/sbin/lpadmincern -u
%endif

exit 0

%preun
%if 0%{?rhel} > 7
if [ $1 -eq 0 ] ; then
        # Package removal, not upgrade
        systemctl --no-reload disable --now lpadmincern.timer &>/dev/null || :
fi
%endif

if [ "$1" = "0" ]; then
        /usr/sbin/alternatives --remove print %{_bindir}/lpr.cern
fi
exit 0

%triggerin -- cups
if [ -x /bin/systemctl ]; then
 /bin/systemctl enable cups 2>&1 >> /dev/null ||:
 /bin/systemctl start cups 2>&1 >> /dev/null ||:
else
 /sbin/chkconfig --add cups 2>&1 >> /dev/null ||:
 /sbin/service cups restart 2>&1 >> /dev/null ||:
fi

%postun
%if 0%{?rhel} > 7
if [ $1 -ge 1 ] ; then
        # Package upgrade, not uninstall
        systemctl try-restart lpadmincern.timer &>/dev/null || :
fi

%endif


#triggerpostun -- xprint-cups
#echo "enabling and starting cups, please wait..."
#/sbin/chkconfig --add cups 2>&1 >> /dev/null ||:
#/sbin/service cups restart 2>&1 >> /dev/null ||:
#echo "Updating all printers definitions, please wait..."
#/usr/sbin/lpadmincern -u
#exit 0
