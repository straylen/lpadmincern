#!/usr/bin/perl -w

#
# lookup CERN LDAP for printer info and setup local CUPS spooler with right printer options.
#
# Mar 2010 - Feb 2014 Jaroslaw.Polok <jaroslaw.polok@cern.ch>

use strict;
use Getopt::Long;
use Pod::Usage;
use Net::LDAP;
use Net::LDAP::Control::Paged;
use Net::LDAP::Constant qw( LDAP_CONTROL_PAGED );

my $shortusage="Usage: lpadmincern PRINTERNAME [--add|--remove|--list|--update]\n See: lpadmincern --help or man lpadmincern for more information\n";
my ($format,$duplex,$add,$remove,$update,$debug,$help,$list,$color,$paper,$model,$name,$default,$server);
my @allprinters = ();
my @localCERNprinters = ();
my @building = ();
my %modelbindings = ();


#################################
#
# restart CUPSd when done
#
#################################
sub reloadcupsd {
    my $cupsc;
    if ( -x '/usr/bin/systemctl' ) {
       # is this actually needed ?
       $cupsc='/usr/bin/systemctl restart cups';
    } else {
       $cupsc='/sbin/service cups reload';
    }
    printf "running: $cupsc\n" if $debug;
    system($cupsc);
    die "Failed to execute: $cupsc\n" if ($? == -1);
    die "Error executing: $cupsc\n" if ($? >> 8);
}


#################################
#
# manage all different duplexes
#
#################################
sub setduplex {
    my ($prt)=@_;
    if ($prt->{printDuplexSupported} eq "yes") {
        #
        # OK, so HOW actually are these known ?
        # install all your printers with PPDs then try:
        # for P in $(lpstat -p | awk '{print $2}'); do lpoptions -p $P -l | grep -i duplex | awk -F "/" '{print $1}';  done
        #
        return "-o Duplex=DuplexNoTumble -o Duplexer=True -o DuplexUnit=Installed -o HPOption_Duplexer=True -o OKOptionDuplex=True -o Option1=True -o Option3=True -o XRXOptionDuplex=True -o DefaultDuplex=DuplexNoTumble";
    } else {
        return "";
    }
}

#################################
#
# find PPD for given printer ..
#
#################################
sub modelppdbindings {
    my $ppds="/usr/share/lpadmincern/ppds/";
    #$ppds="./ppds/";
    my %models =();

    opendir(PPDS, $ppds) || die "Error: $ppds directory missing.";
    while (my $ppd=readdir (PPDS)) {
        open (PPD, "$ppds/$ppd") || die "Error: cannot open: $ppds/$ppd for reading";
        my @content = <PPD>;
        close (PPD);
        my @datainfo= grep { /^\*ModelName/ } @content;
        foreach my $data (@datainfo) {
            $data =~s/^\*ModelName:\s+\"(.*)\".*$/$1/;
            chomp($data);
            printf "Printer Model: $data PPD: $ppd\n" if $debug;
            $models{$data}="$ppds/$ppd";
        }
    }
    closedir PPDS;
    return %models;
}

#################################
#
# Bind model names to PPDs
#
#################################
sub printerdriverbindings {
    my @allprinters=@_;
    my %modelbindings = modelppdbindings();


    my %nametweaks = (
            #LDAP returns from landb        Model in PPD file
			"CANON IR1730"		, "Canon iR1730i PS3",
			"CANON IR-ADV C5030"	, "Canon iR-ADV C5030/5035 PS",
			"CANON IR-ADV 4225"	, "Canon iR-ADV 4225/4235 PS",
			"CANON IR C3080I"        	, "Canon iR C3080/3480/3580 PS",
			"CANON IR C3080"          , "Canon iR C3080/3480/3580 PS",
			"CANON IR2270"  		, "Canon iR2270/iR2870 PS",
			"CANON IR C 2380 I"	, "Canon iR C2380/2550 PS",
			"CANON IR C2380"          , "Canon iR C2380/2550 PS",
			"CANON IR1022A"		, "Canon iR1018-1022 CPCA",
			"CANON IR1024I"		, "Canon iR1018-1022 CPCA",  #VERIFY
			"CANON IR1024"            , "Canon iR1018-1022 CPCA",  #VERIFY
			"CANON IR3035"		, "Canon iR3035/iR3045 PS",
			"CANON IR3045"            , "Canon iR3035/iR3045 PS",
			"CANON IRC 3380I"		, "Canon iR C2880/C3380 PS",
			"CANON IR C3380"          , "Canon iR C2880/C3380 PS",
			"CANON IRC 2880 I"	, "Canon iR C2880/C3380 PS",
			"CANON IR C2880"          , "Canon iR C2880/C3380 PS",
			"CANON IR3300I"		, "Canon iR2200-3300 PS",
			"CANON IR-ADV 4245"	, "Canon iR-ADV 4245/4251 PS",
			"CANON IR-ADV 4245"	, "Canon iR-ADV 4245/4251 PS",
			"CANON IR4570"		, "Canon iR3570/iR4570 PS",
			"CANON IR3570"		, "Canon iR3570/iR4570 PS",
			"CANON IR3245"		, "Canon iR3235/iR3245 PS",
			"CANON IR 3245N"		, "Canon iR3235/iR3245 PS",
			"CANON IR3235N/E"		, "Canon iR3235/iR3245 PS",
			"CANON IR3235"            , "Canon iR3235/iR3245 PS",
			"CANON IR 5055N"		, "Canon iR5055/iR5065 PS",
			"CANON IR5055"            , "Canon iR5055/iR5065 PS",
			"CANON IR 6570"		, "Canon iR5570/iR6570 PS",
			"CANON IR6570"            , "Canon iR5570/iR6570 PS",
			"CANON IR 5870"		, "Canon iR C5870C EUR PS",
			"CANON IR 5870C"          , "Canon iR C5870C EUR PS",
			"CANON IR 2570C EUR"	, "Canon iR 2570C EUR PS",
			"CANON IR 2570C"          , "Canon iR 2570C EUR PS",
			"CANON LBP6650"		, "Canon LBP6650/3470 PS",

			"CANON IR-ADV C5535"	, "Canon iR-ADV C5535/5540 PS",
			"CANON IR-ADV C5235"	, "Canon iR-ADV C5235/5240 PS",
			"CANON IR-ADV 4025"	, "Canon iR-ADV 4025/4035 PS",
			"CANON IR-ADV 4045"	, "Canon iR-ADV 4045/4051 PS",
			"CANON IR3225"            , "Canon iR3225 PS",
			"CANON IR-ADV 400"	, "Canon iR-ADV 400/500 PS",
			"CANON IR-ADV C5535I"     , "Canon iR-ADV C5535/5540 PS",
			"CANON IR-ADV C356I"      , "Canon iR-ADV C256/356 UFR II",
			"CANON IR-ADV C356"      , "Canon iR-ADV C256/356 UFR II",
			"CANON IR-ADV 4535"       , "Canon iR-ADV 4525/4535 III UFR II",
			"CANON IR-ADV 525"       , "Canon iR-ADV 525 UFR II",
			"HP LASERJET 500 COLOR M551", "HP LaserJet 500 color M551",
			"HP COLOR LASERJET M553"	, "HP Color LaserJet M553",
			"HP COLOR LASERJET CP5520 SERIES", "HP Color LaserJet CP5520 Series",
			"HP LASERJET 600 M603"    , "HP LaserJet 600 M601 M602 M603",
			"HP Laserjet P2055DN"     , "HP LaserJet P2055 Series",
			"HP LASERJET P2055DN"     , "HP LaserJet P2055 Series",
			"HP LASERJET 2200"        , "HP LaserJet 2200",
			"HP LASERJET P2015 SERIES", "HP LaserJet P2015 Series",
			"HP LASERJET P3004"	, "HP LaserJet P3004",
			"HP LASERJET P4014"	, "HP LaserJet P4010 Series",
			"HP LASERJET 4300"        , "HP LaserJet 4300 Series",
			"HP LASERJET 4100 SERIES"	, "HP LaserJet 4100 Series ", # trailing space !
			"HP LASERJET P3010 SERIES", "HP LaserJet P3010 Series",
			"HP LASERJET 2430PCL6"	, "HP LaserJet 2430",
			"HP LASERJET 2300 DTN"	, "HP LaserJet 2300",
			"HP LASERJET 2300 SERIES" , "HP LaserJet 2300",
			"HP COLOR LASERJET CP 6015 XH","HP Color LaserJet CP6015",
			"HP COLOR LASERJET CM4730 MFP", "HP Color LaserJet 4730mfp",
			"HP BUSINESS INKJET 2800"	, "HP Business Inkjet 2800 PS",
			"HP BUISNESS INKJET 2800"	, "HP Business Inkjet 2800 PS",  #(sp.!)

			"HP LASERJET IIISI POSTSCRIPT","HP LaserJet 3",
			"HP LASERJET 5M"		, "HP LaserJet 5/5M",
			"HP LASERJET 5SI"         , "HP LaserJet 5Si/5Si MX",
			"HP LASERJET 6MP"		, "HP LaserJet 6P/6MP",
			#   "HP LASERJET P3005PS"	, "HP LaserJet P3005",
			"HP LASERJET P3015DN"	, "HP LaserJet 3015",  
			"HP LASERJET 2100 TN"	, "HP LaserJet 2100 Series",
			"HP LASERJET 2200 SERIES" , "HP LaserJet 2200",
			"HP LASERJET 4050 SERIES" , "HP LaserJet 4000 Series",
			"HP LASERJET 4250"        , "HP LaserJet 4200",

			"HP LASERJET P4515"       , "HP LaserJet P4010 Series",      
			"HP LASERJET 700 M712"	, "HP LaserJet 700 M712",
			"HP COLOR LASERJET 2840"	, "HP Color LaserJet 2800",
			"HP LASERJET 5100 SERIES" , "HP LaserJet 5100",
			"HP LASERJET 8150 SERIES"	, "HP LaserJet 8150 Series PS",
			"HP HP COLOR LASERJET 4550", "HP Color LaserJet 4550 ",  # trailing space !

			"HP COLOR LASERJET 5500"	, "HP Color LaserJet 5500 PS",

			"HP 2500C SERIES"		, "HP DesignJet 2500CP PS3",
			"HP DESIGNJET 2500CP"	, "HP DesignJet 2500CP PS3",
			"HP DESIGNJET 650C .C2859A.", "HP DesignJet 650C",
			"HP DESIGNJET 650C .C2859B.", "HP DesignJet 650C",
			"HP DESIGNJET 750C"	, "HP DesignJet 750C Plus",
			"HP DESIGNJET 755C"	, "HP DesignJet 750C Plus",
			"HP DESIGNJET 755CM"	, "HP DesignJet 750C Plus",
			"HP DESIGNJET 1055CM"	, "HP DesignJet 1055CM PS3",

			"HP LASERJET M606"	, "HP Laserjet M604 M605 M606",
			"HP LASERJET M506"	, "HP LaserJet M506",

			"INFOTEC ISC 2428"	, "infotec  ISC 2428",

			"OCE 3165"		, "Oce 3165 PS3",
					  "OCE TDS450"              , "Oce TDS450 PS",
					  "OCE TCS500 COLOUR"       , "Oce TCS500 PS",
					  "OCE TCS500"              , "Oce TCS500 PS",
					  "OCE TDS600"              , "Oce TDS600 PS",
					  "OCE TDS700"              , "Oce TDS700 PS",

					  "TEKTRONIX PHASER 750 DX"	, "Tektronix Phaser 750DP",
					  "TEKTRONIX PHASER 750DX"  , "Tektronix Phaser 750DP",
					  "TEKTRONIX PHASER 750"	, "Tektronix Phaser 750DP",
					  "TEKTRONIX PHASER 850DP"  , "Tektronix Phaser 860DP",
					  "TEKTRONIX PHASER 8200DP" , "Xerox Phaser 8200DP",  

					  "XEROX DOCUMENT CENTRE 440", "Xerox Document Centre 440 PS",
					  "XEROX DOCUMENT CENTRE 535", "Xerox Generic", # Xerox could really try harder
					  "XEROX WORKCENTRE 255"     , "Xerox Generic", # Xerox could really try harder
					  "XEROX WORKCENTRE PRO 255" , "Xerox Generic", # Xerox could really try harder
					  "XEROX WORKCENTRE 245 PRO" , "Xerox Generic", # Xerox could really try harder
					  "XEROX WORKCENTRE PRO 245" , "Xerox Generic", # Xerox could really try harder
					  "XEROX WORKCENTRE 7245"    , "Xerox Generic", # Xerox could really try harder
					  "XEROX PHASER 860"         , "Xerox Generic", # Xerox could really try harder
					  "XEROX PHASER 7300 DN"     , "Xerox Phaser 7300DN",
					  "XEROX PHASER 4510N"       , "Xerox Generic", # Xerox could really try harder
					  "XEROX COLORQUBE 9203"     , "Xerox ColorQube 9201/9202/9203",


				  );

	# We have no idea about:
	#	Generic 75/60BW-1 PS(V)
	#	Oce MP1050
	#   Oce CW300
    #   Plotvawe 300
    # ... so we map all the above to generic postscript

    foreach my $nametweak (keys %nametweaks) {
        $modelbindings{$nametweak}=$modelbindings{$nametweaks{$nametweak}};
        #print "->$nametweak<-->$nametweaks{$nametweak}<-\n" if $debug;
     }

    for my $printer (@allprinters) {
        $printer->{ppdfile}=$modelbindings{"Generic PostScript Printer"};
        $printer->{driver}="GENERIC POSTSCRIPT PRINTER";

        for my $match (keys %modelbindings) {
            if ($match =~ /^$printer->{description}$/i || $printer->{description} =~ /^$match$/i)  {
                $printer->{ppdfile}=$modelbindings{$match};
                $printer->{driver}=$match;
                last;
            }
        }

        printf "Binding: %s = %s -> %s [%s]\n",$printer->{printerName},$printer->{description},$printer->{driver},$printer->{ppdfile} if $debug;
    }
    #exit 1;
    return @allprinters;
}

#################################
#
# generate list of CERN printers defined
#
#################################
sub getlocalCERNprinters {
    my @allprinters = @_;
    my @localprinters = ();
    # This could also be done by parsing 'lpstat -v' output, however is
    # prone to error
    if (open my $fh, "<", "/etc/cups/printers.conf") {
        while (my $row = <$fh>) {
            if($row =~ m/<Printer (.*)>/) {
                for (@allprinters) {
                    if ($_->{printerName} eq $1) {
	                push(@localprinters, $_);
                    }
                }
            }
        }
   }
   return @localprinters;
}

#################################
#
# query the LDAP
#
#################################
sub ldapsearch {
    my ($search)=@_;
    my $endline="No match found.\n";
    my $lastpage=0;
    my @entries;
    my $cookie;

    printf "LDAP search: %s\n",$search if $debug;

    my $ldap = Net::LDAP->new ( "xldap.cern.ch" ) or die "Error connecting to LDAP: $@\n";
    my $mesg = $ldap->bind ( version => 3,  multihomed => 1, timeout => 15) or die "Error binding to LDAP: $@\n";
    my $page = Net::LDAP::Control::Paged->new( size => 1000 ); # 1000 is max anyway on ldap servers we have.
    my @searchargs =(
                     base    => "ou=print servers,ou=system infrastructure,ou=cern main servers,dc=cern,dc=ch",
                     scope   => "sub",
                     filter  => "$search",
                     attrs   => "",
                     control => [ $page ],
                     #	sizelimit => 4000,
                    );

    while($lastpage < 1) {
        my $result = $ldap->search (@searchargs) or die "Error searching LDAP: $@\n";
        $result->code and last;
        my($resp)  = $result->control( LDAP_CONTROL_PAGED ) or last;

        # OK, the MS server misbehaves here:
        # cookie shall indicate that there is no more data - instead it indicates that next page is not full
        # however: if we ignore it, server goes in a loop and keeps on feeding us same result pages again and
        # again .... oh well, we will terminate the loop ourselves then 

        $cookie  = $resp->cookie or $lastpage=1;
        $page->cookie($cookie);
        push (@entries, $result->entries); 
    }


    if ($cookie) {
        $page->cookie($cookie);
        $page->size(0);
        $ldap->search( @searchargs );
    }

    printf "LDAP return: %d entries.\n",$#entries +1 if $debug;
    return @entries;
}

##############################
#
# Read options for the printer
#
##############################
sub getlpoptions {
    my ($prt)=@_;
    my $command="LC_ALL=C /usr/bin/lpoptions -p ".$prt->{printerName}." -l";
    my %setopts;

    print "Running: $command\n" if $debug;

    foreach my $opt (`$command`) {
        #optionname/optionguiname: val1 val2 *val3 val4
        next if $opt !~ /^(\w+).*\*(\S+)\s.*$/;
        $setopts{$1}=$2;
    }

    return %setopts;
}

##############################
#
# Write options for the printer
#
##############################
sub setlpoptions {
    my ($prt,%setopts)=@_;
    my $command="LC_ALL=C /usr/bin/lpoptions -p ".$prt->{printerName};
    my $args="";

    foreach my $opt (keys %setopts) {
        $args.=" -o $opt=$setopts{$opt}";
    }
    if ($args ne "") {
        print "Running: $command $args\n" if $debug;
        system('$command $args');
    }
}

##############################
#
# Add new printer
#
##############################
sub addprinter {
    my ($prt,$server,$update,%setopts)=@_;
    my ($duplex,$media,$command,$uri);

    $command="LC_ALL=C /usr/bin/lpstat -p ".$prt->{printerName}." 2>&1";

    print "Running: $command\n" if ($debug);

    return if (!$update && `$command` =~ /printer $prt->{printerName} /);

    if($update) { print "Updating" } else { print "Adding" }
    print " printer: ".$prt->{printerName}."\n";
    if ($server) {
        # use central printserver
        $uri="\"lpd://$prt->{serverName}/$prt->{printerName}\"";
#
#        We could use IPP .. but it requires Kerberos/Basic auth 
#        on CERN printservers ...
#
#        $uri="\"http://$prt->{serverName}/printers/$prt->{printerName}\"";
#
#
    } else {
        # go to printer directly
        $uri="\"lpd://b$prt->{printerName}\"";
    }

    $duplex=setduplex($prt);
    #if ($printer->{printMediaReady} =~ /A4/ ) { $media="-o PageSize=A4";} else { $media="" };
    $media="-o PageSize=A4";

    $command="LC_ALL=C /usr/sbin/lpadmin -p $prt->{printerName} -L \"$prt->{location}\" -P $prt->{ppdfile} -D \"$prt->{description}\" -v $uri $duplex $media -o printer-is-shared=false -E";

    print "Running: $command\n" if ($debug);

    system($command);

    if (%setopts) {
        setlpoptions($prt,%setopts);
    }
}

##############################
#
# Remove printer
#
##############################
sub delprinter {
    my ($prt,$update)=@_;
    my $command="LC_ALL=C /usr/bin/lpstat -p ".$prt->{printerName}." 2>&1";
    my %setopts;

    print "Running: $command\n" if $debug;

    if (`$command` =~ /^printer $prt->{printerName} /) {
        %setopts=getlpoptions($prt);
        return (1,%setopts) if $update;
        print "Removing printer: $prt->{printerName}\n"; 
        $command="LC_ALL=C /usr/sbin/lpadmin -x ".$prt->{printerName};
        print "Running: $command\n" if ($debug);
        system($command);
        return (1,%setopts);
    }
    return (0,%setopts);
}

##############################
#
# Get default printer
#
##############################
sub getdefprinter {
    my ($command);
    $command="LC_ALL=C /usr/bin/lpstat -d";

    print "Running: $command\n" if $debug;

    if (`$command` =~ /^system\sdefault\sdestination.\s+(.*)/) {
        print "System default printer: $1\n" if $debug;
        return $1;
    }
    return "";
}

##############################
#
# Set default printer
#
##############################
sub setdefprinter {
    my ($prt)=@_;
    my $command="LC_ALL=C /usr/bin/lpstat -p ".$prt;
    return if ($prt eq "");

    if (`$command` !~ /Unknown\sdestination/){
        print "Setting default printer to: $prt ...\n";
        $command="/usr/sbin/lpadmin -d ".$prt;
        print "Running: $command\n" if $debug;
        system($command);
    } else {
        print "Error: cannot set default printer to: $prt (no such printer)\n";
        exit 1;
    }
}


#############################
#
# Lets start the job
#
#############################

$color=-1;
$duplex=-1;
$server=1;

my %opts=(
          "building=s"    => \@building,
          "format=s"      => \$format,
          "model=s"       => \$model,
          "duplex!"       => \$duplex,
          "color!"        => \$color,
          "add|install"   => \$add,
          "remove"        => \$remove,
          "update"        => \$update,
          "list"          => \$list,
          "paper=s"	 => \$paper,
          "verbose"       => \$debug,
          "default=s"     => \$default,
          "name=s"        => \$name,
          "help"          => \$help,
          "server!"	 => \$server,
         );

my $search="";
my $ok =0;
my $options = GetOptions(%opts);
my $setdef = 0;

pod2usage(1) if $help;

$setdef = 1 if $default;

if (($add||0)+($remove||0)+($update||0)+($list||0)+($setdef||0) > 1) {
    print "--add --remove --update --list --default options are mutually exclusive.\n";
    print "see lpadmincern --help for more information\n";
    exit 1;
}

my $what = "list";

$what="add" if $add;
$what="remove" if $remove;
$what="update" if $update;
$what="default" if $default;

if ($what ne "list") {
    if ($< != 0) { print "You must be root in order to add/remove/update/set default printer(s)\n"; exit 1; }
}

while (@ARGV){
    $search.="(printerName=$ARGV[0])";$ok+=1;shift @ARGV;
}
$search="(|".$search.")" if $ok>1;

$search.="(printerName=$name)",$ok+=1 if $name;

if (defined ($#building)) {
  $search.="(\|" if ($#building>0);
  foreach (@building) {
    $search.="(location=$_ *)";
  }
  $search.=")" if ($#building>0);
  $ok+=1;
}

$search.="(printMediaSupported=$format*)",$ok+=1 if $format;
$search.="(description=$model*)",$ok+=1 if $model;
$search.="(printColor=TRUE)",$ok+=1 if $color > 0;
$search.="(printColor=FALSE)",$ok+=1 if !$color && $color!=-1 ;
$search.="(printDuplexSupported=TRUE)",$ok+=1 if $duplex>0;
$search.="(printDuplexSupported=FALSE)",$ok+=1 if !$duplex && $duplex!=-1;
$search.="(printMediaSupported=*$paper*)",$ok+=1 if $paper;
$search.="(printerName=*)",$ok+=1 if !$ok && $update;
$search.="(printerName=*)",$ok+=1 if !$ok && $default;
if(!$ok) { printf $shortusage;exit 1;}
$search.="(objectClass=printQueue)";$ok+=1;
$search="(&".$search.")" if $ok > 1;


#$endline.="[output truncated - maximum number of LDAP records reached (1000).]\n" if ($#entries + 1 == 1000) ;
my $endline="No match found.";

my @entries=ldapsearch($search);

if($#entries + 1){
    if ($#entries == 0){
        $endline="1 match found.";
    } else {
        $endline=$#entries + 1 ." matches found.";
    }
}

foreach my $entr ( @entries ) {
    printf "--LDAP entry--\n" if $debug;
    my $attr;
    my %printer=( printerName => undef,
                  location => undef,
                  description => undef,
                  serverName => undef,
                );
    my @printermedia= ();
    foreach $attr ( sort $entr->attributes ) {
        #this would have even worked if that wouldn;t be an MS ldap ...
        next if ( $attr =~ /;binary$/ );

        if ($debug) {
            print $attr."=>";
            my @out1=$entr->get_value ( $attr );
            foreach my $out (@out1) { 
                if ($out!~/^[[:ascii:]]+$/) { print "0x".unpack("H*",$out)."\n"; } else { print $out."\n"; }
            }
        }

        for ($attr) {

            /^printerName$/
                && do { $printer{printerName}=$entr->get_value ( $attr ); last;};
            /^location$/
                && do { $printer{location}=$entr->get_value ( $attr ); last;};
            /^description$/
                && do { $printer{description}=$entr->get_value ( $attr );last;};

            /^printColor$/
                && do { if($entr->get_value($attr) eq "TRUE") {
                            $printer{printColor}="yes"
                        } else {
                            $printer{printColor}="no"
                        }
                        last;};
            /^printDuplexSupported$/
                && do { if($entr->get_value($attr) eq "TRUE") {
                            $printer{printDuplexSupported}="yes"
                        } else {
                            $printer{printDuplexSupported}="no"
                        }
                        last;};
            /^serverName$/
                && do { $printer{serverName}=$entr->get_value ( $attr ); last;};
            /^portName$/
                && do { $printer{portName}=$entr->get_value ( $attr ); last;};
            /^printMediaReady$/
                && do { push(@printermedia,$entr->get_value ( $attr )); last;};
        }
    }
    warn "Rejected data: printerName undefined (LDAP object is not a printer?)\n",next if (!$printer{printerName});

    $printer{printMediaReady}=join(",",@printermedia);
    if (defined $printer{description} &&
        defined $printer{location} &&
        defined $printer{serverName} &&
        $printer{description} !~/^\s+$/) {
        push(@allprinters,{%printer});
    } else {
        warn "Rejected data for: $printer{printerName} (incomplete LDAP object).\n";
        next;
    }
}



if ($#entries<0) {
    print $endline."\n";
    exit 0;
}

@allprinters=printerdriverbindings(@allprinters);
@localCERNprinters=getlocalCERNprinters(@allprinters);

if ($what eq "list") {
    printf "%-22s %-31s %-11s %3s %3s\n", "Printer name","Printer model","Location","Color","Duplex";
    printf "-" x 80 . "\n";

    for my $printer (@allprinters) {
        my $drv="!";
        if ($printer->{driver} !~/^Generic PostScript Printer$/i) { $drv=""; }
        printf "%-22s %-31s %-12s %3s  %3s %1s\n",
            $printer->{printerName},
            $printer->{description},
            $printer->{location},
            $printer->{printColor},
            $printer->{printDuplexSupported},
            $drv;
		    #$printer->{printMediaReady};
    }

    printf "-" x 80 . "\n";
    print $endline." (! in last column: generic postscript driver used).\n";
}


if ($what eq "add") {
    for my $printer (@allprinters) {
        addprinter($printer,$server,0);
    }
}

if ($what eq "remove") {
    for my $printer (@allprinters) {
        delprinter($printer,0);
    }
}


if ($what eq "update") {
    my $defprt=getdefprinter();
    for my $printer (@localCERNprinters) {
        my ($res,%printeropts)=delprinter($printer,1);

        if ($res) {
            addprinter($printer,$server,1,%printeropts);
		}
	}
    setdefprinter($defprt);
}

if ($default) {
    setdefprinter($default);
}

reloadcupsd() if ($what ne "list");

__END__

=pod

=head1 NAME

lpadmincern- - Utility to setup CERN printers.

=head1 DESCRIPTION

lpadmincern is a command line tool to ease adding of CERN printers to
local system setup.
It will search CERN LDAP database for printer information and display
the output or add/remove/update system printers according to search
criteria defined.

=head1 SYNOPSIS

=over 2

lpadmincern [--help]

lpadmincern [--name] SEARCHSTRING [--list]

lpadmincern [--name] [SEARCHSTRING] [--model MD ] [--building BD]
            [--color|--nocolor] [--duplex|--noduplex] [--paper PS]
            [--add|--remove|--update|--list|--default PRT]

=back

=head1 OPTIONS

=over 4

=item B<--help>

Shows this help desription

=item B<--name> PRINTERNAME

Search for printers matching PRINTERNAME (option may be ommitted).

=item B<--model> MODEL

Search for printers of MODEL.

=item B<--building> BUILDING

Search for printers in BUILDING. (option can be specified multiple times)

=item B<--color|--nocolor>

Search for Color or Monochrome printers only.

=item B<--duplex|--noduplex>

Search for DUPLEX printing capable printers.

=item B<--paper> PAPERSIZE

Search for printers allwoing PAPERSIZE (A4,A3,Custom ..).

=item B<--list>

Show matching printers on screen.

=item B<--add>

Add matching printers to the system.

=item B<--remove>

Remove matching printers from system.

=item B<--update>

Update matching printer definitions on the system.

=item B<--default> PRINTER

Set default system printer to PRINTER

=back

All options can be abbreviated to shortest distinctive lenght,
(first letter). Single minus preceding option name may be used
instead of double one.

For wildcard search use * character (double quoted or back-slashed
to avoid shell variable expansion: "*" or \* ).

Please note that search option arguments are case insensitive.

=head1 DESCRIPTION

lpadmincern performs a search in CERN LDAP database, according to
search criteria defined by options and installs/updates/removes
named printers from the system.

=head1 EXAMPLES

lpadmincern [--name] "*COR*CAN*" [--list]

lpadmincern --building 31 --add

lpadmincern --paper A3 --color --duplex [--list]

lpadmincern --remove "*"

lpadmincern --update --building 31

=head1 AUTHORS

Jaroslaw Polok <jaroslaw.polok@cern.ch>, Ben Morrice <ben.morrice@cern.ch>

=head1 EXIT STATUS

Normally lpadmincern will return with exit code 0, indicating a successful run

In the event that no printers are found via an ldap search, the exit code will be 10

In the event that communication to ldap fails, the exit code will be 50

In all other error conditions, the exit code will be 1


=head1 KNOWN BUGS

No checking of lpadmin / lpstat / lpoptions return code ...

Also we are eager to here about any (other) bugs at linux.support@cern.ch

=cut

