*PPD-Adobe: "4.3"
*%
*%  Copyright CANON INC. 2017
*%  CUPS printer driver for Canon printer devices
*%
*%  This program is free software; you can redistribute it and/or modify
*%  it under the terms of the GNU General Public License as published by
*%  the Free Software Foundation; either version 2 of the License, or
*%  (at your option) any later version.
*%
*%  This program is distributed in the hope that it will be useful,
*%  but WITHOUT ANY WARRANTY; without even the implied warranty of
*%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*%  GNU General Public License for more details.
*%
*%  You should have received a copy of the GNU General Public License
*%  along with this program; if not, write to the Free Software
*%  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*%

*FormatVersion: "4.3"
*FileVersion: "3.5"
*LanguageVersion: English
*LanguageEncoding: ISOLatin1
*PCFileName: "CNC256ZK.PPD"
*Manufacturer: "Canon"
*Product: "(ir-adv c256/356)"
*cupsVersion: 1.1
*cupsManualCopies: False
*cupsModelNumber: 8001
*%
*% Removing cups filter: these printers are PostScript
*% jaroslaw.polok@cern.ch, 07.05.2018
*%
*%cupsFilter: "application/vnd.cups-postscript 0 pstoufr2cpca"
*ModelName: "Canon iR-ADV C256/356 UFR II"
*ShortNickName: "iR-ADV C256/356"
*NickName: "Canon iR-ADV C256/356 UFR II"
*PSVersion: "(3010.000) 550"
*PSVersion: "(3010.000) 651"
*LanguageLevel: "3"
*ColorDevice: True
*DefaultColorSpace: RGB
*FileSystem: False
*Throughput: "71"
*LandscapeOrientation: Plus90
*VariablePaperSize: False
*TTRasterizer: Type42
*%CNGPLPLIBNAME: "uictlufr2"
*%CNGPLPLIBNAMEVER: "1.0.0"

*%CNPrintLang: UFR2

*opvpDevice: "opvp"
*opvpDriver: "libcanonc3pl"
*opvpModel: "iR-ADVC256/356"

*OpenGroup: InstallableOptions/Attached Options

*OpenUI *CNFinisher/Output Option: PickOne
*DefaultCNFinisher: None
*CNFinisher None/Not Installed: "<< >>setpagedevice"
*CNFinisher STFINZ1/Staple Finisher-Z1: "<< >>setpagedevice"
*CloseUI: *CNFinisher

*OpenUI *CNEnableTrustPrint/Secure Printing Function of Device: Boolean
*DefaultCNEnableTrustPrint: False
*CNEnableTrustPrint False/Off: "<< >>setpagedevice"
*CNEnableTrustPrint True/On: "<< >>setpagedevice"
*CloseUI: *CNEnableTrustPrint

*CloseGroup: InstallableOptions

*OpenUI *Resolution/Resolution : PickOne
*DefaultResolution: 600dpi
*Resolution 600dpi/600 dpi: "<</HWResolution[600 600]>>setpagedevice"
*CloseUI: *Resolution

*OpenUI *CNMatchingMethod/Matching Method: PickOne
*DefaultCNMatchingMethod: None
*CNMatchingMethod None/Printer Default: ""
*CNMatchingMethod Photographic/General: ""
*CNMatchingMethod Monitor/Perceptual: ""
*CNMatchingMethod Colorimetric/Colorimetric: ""
*CNMatchingMethod Vividphoto/Vivid Photo: ""
*CNMatchingMethod CustomColor_1/Special Red: ""
*CloseUI: *CNMatchingMethod

*OpenUI *CNTonerSaving/Toner Save : PickOne
*DefaultCNTonerSaving: False
*CNTonerSaving Auto/Auto: "<< >>setpagedevice"
*CNTonerSaving True/ON: "<< >>setpagedevice"
*CNTonerSaving False/OFF: "<< >>setpagedevice"
*CloseUI: *CNTonerSaving

*OpenUI *MediaType/Media Type : PickOne
*DefaultMediaType: Auto
*MediaType Auto/Auto: "<</MediaType(Auto)>>setpagedevice"
*MediaType PlainPaper1/Plain Paper 1: "<</MediaType(PlainPaper1)>>setpagedevice"
*MediaType PlainPaper2/Plain Paper 2: "<</MediaType(PlainPaper2)>>setpagedevice"
*MediaType PlainPaper3/Plain Paper 3: "<</MediaType(PlainPaper3)>>setpagedevice"
*MediaType RECYCLED1/Recycled Paper 1: "<</MediaType(RECYCLED1)>>setpagedevice"
*MediaType RECYCLED2/Recycled Paper 2: "<</MediaType(RECYCLED2)>>setpagedevice"
*MediaType RECYCLED3/Recycled Paper 3: "<</MediaType(RECYCLED3)>>setpagedevice"
*MediaType COLOR/Color Paper: "<</MediaType(COLOR)>>setpagedevice"
*MediaType PREPUNCHED/Pre-Punched Paper: "<</MediaType(PREPUNCHED)>>setpagedevice"
*MediaType BOND/Bond Paper: "<</MediaType(BOND)>>setpagedevice"
*MediaType HEAVY1/Heavy Paper 1: "<</MediaType(HEAVY1)>>setpagedevice"
*MediaType HEAVY2/Heavy Paper 2: "<</MediaType(HEAVY2)>>setpagedevice"
*MediaType HEAVY3/Heavy Paper 3: "<</MediaType(HEAVY3)>>setpagedevice"
*MediaType HEAVY4/Heavy Paper 4: "<</MediaType(HEAVY4)>>setpagedevice"
*MediaType HEAVY5/Heavy Paper 5: "<</MediaType(HEAVY5)>>setpagedevice"
*MediaType OHP/OHP: "<</MediaType(OHP)>>setpagedevice"
*MediaType LABELS/Labels: "<</MediaType(LABELS)>>setpagedevice"
*MediaType PlainPaperL/Thin: "<</MediaType(PlainPaperL)>>setpagedevice"
*MediaType ENVELOPE/Envelope: "<</MediaType(ENVELOPE)>>setpagedevice"
*CloseUI: *MediaType

*OpenUI *InputSlot/Paper Source: PickOne
*DefaultInputSlot: Auto
*InputSlot Auto/Auto : ""
*InputSlot Manual/Multi-purpose Tray : ""
*InputSlot Cas1/Drawer 1 : ""
*InputSlot Cas2/Drawer 2 : ""
*InputSlot Cas3/Drawer 3 : ""
*InputSlot Cas4/Drawer 4 : ""
*CloseUI: *InputSlot

*OpenUI *OutputBin/Paper Destination: PickOne
*DefaultOutputBin: Auto
*OutputBin Auto/Auto : "<< >>setpagedevice"
*CloseUI: *OutputBin

*OpenUI *Duplex/Duplex: PickOne
*DefaultDuplex: None
*Duplex None/OFF: "<</Duplex false>>setpagedevice"
*Duplex DuplexNoTumble/ON (Long-edged Binding): "<</Duplex true/Tumble false>>setpagedevice"
*Duplex DuplexTumble/ON (Short-edged Binding): "<</Duplex true/Tumble true>>setpagedevice"
*CloseUI: *Duplex

*OpenUI *BindEdge/BindingEdge: PickOne
*DefaultBindEdge: Left
*BindEdge Left/Left: "<< >>setpagedevice"
*BindEdge Top/Top: "<< >>setpagedevice"
*CloseUI: *BindEdge

*OpenUI *CNOutputPartition/Output Partition: PickOne
*DefaultCNOutputPartition: None
*CNOutputPartition None/None: "<< >>setpagedevice"
*CNOutputPartition offset/Offset: "<< >>setpagedevice"
*CloseUI: *CNOutputPartition

*OpenUI *Collate/Collate: PickOne
*DefaultCollate: True
*Collate True/Collate: "<< >>setpagedevice"
*Collate Group/Group: "<< >>setpagedevice"
*Collate StapleCollate/Staple & Collate: "<< >>setpagedevice"
*Collate StapleGroup/Staple & Group: "<< >>setpagedevice"
*CloseUI: *Collate

*OpenUI *StapleLocation/Staple Location: PickOne
*DefaultStapleLocation: TopLeft
*StapleLocation None/No Stapling : "<< >>setpagedevice"
*StapleLocation TopLeft/Upper Left (Single) : "<< >>setpagedevice"
*StapleLocation TopRight/Upper Right (Single) : "<< >>setpagedevice"
*StapleLocation BottomLeft/Lower Left (Single) : "<< >>setpagedevice"
*CloseUI: *StapleLocation

*OpenUI *CNColorMode/Color Mode: PickOne
*DefaultCNColorMode: Auto
*CNColorMode Auto/Auto: "<< >>setpagedevice"
*CNColorMode color/Color: "<< >>setpagedevice"
*CNColorMode mono/Black and White: "<< >>setpagedevice"
*CloseUI: *CNColorMode

*OpenUI *CNNumberOfColors/Number of Colors: PickOne
*DefaultCNNumberOfColors: FullColor
*CNNumberOfColors FullColor/Full Color: "<< >>setpagedevice"
*CNNumberOfColors TwoColors/Two Colors: "<< >>setpagedevice"
*CloseUI: *CNNumberOfColors

*OpenUI *CNColorToUseWithBlack/Color to Use with Black: PickOne
*DefaultCNColorToUseWithBlack: Red
*CNColorToUseWithBlack Red/Red: "<< >>setpagedevice"
*CNColorToUseWithBlack Green/Green: "<< >>setpagedevice"
*CNColorToUseWithBlack Blue/Blue: "<< >>setpagedevice"
*CNColorToUseWithBlack Yellow/Yellow: "<< >>setpagedevice"
*CNColorToUseWithBlack Magenta/Magenta: "<< >>setpagedevice"
*CNColorToUseWithBlack Cyan/Cyan: "<< >>setpagedevice"
*CloseUI: *CNColorToUseWithBlack

*OpenUI *CNCopySetNumbering/Copy Set Numbering: PickOne
*DefaultCNCopySetNumbering: None
*CNCopySetNumbering None/None: "<< >>setpagedevice"
*CNCopySetNumbering True/ON: "<< >>setpagedevice"
*CNCopySetNumbering False/OFF: "<< >>setpagedevice"
*CloseUI: *CNCopySetNumbering

*OpenUI *CNShiftStartPrintPosition/Shift the Position to Start Printing: Boolean
*DefaultCNShiftStartPrintPosition: False
*CNShiftStartPrintPosition True/True: "<< >>setpagedevice"
*CNShiftStartPrintPosition False/False: "<< >>setpagedevice"
*CloseUI: *CNShiftStartPrintPosition

*OpenUI *CNTrustPrint/Use Secure Printing Function of Device: Boolean
*DefaultCNTrustPrint: False
*CNTrustPrint False/Off: "<< >>setpagedevice"
*CNTrustPrint True/On: "<< >>setpagedevice"
*CloseUI: *CNTrustPrint

*OpenUI *CNAdvancedSmoothing/Advanced Smoothing: PickOne
*DefaultCNAdvancedSmoothing: Off
*CNAdvancedSmoothing Off/Off : "<< >>setpagedevice"
*CloseUI: *CNAdvancedSmoothing

*OpenUI *CNColorHalftone/Color Halftones : PickOne
*DefaultCNColorHalftone: None
*CNColorHalftone None/Printer Default: "<< >>setpagedevice"
*CloseUI: *CNColorHalftone

*OpenUI *CNHalftone/Halftones : PickOne
*DefaultCNHalftone: None
*CNHalftone None/Printer Default: "<< >>setpagedevice"
*CloseUI: *CNHalftone

*OpenUI *CNSpecialSmooth/Special Smoothing Mode: PickOne
*DefaultCNSpecialSmooth: None
*CNSpecialSmooth None/Printer Default: "<< >>setpagedevice"
*CloseUI: *CNSpecialSmooth

*OpenUI *CNLineControl/Line Control: PickOne
*DefaultCNLineControl: None
*CNLineControl None/Printer Default: "<< >>setpagedevice"
*CloseUI: *CNLineControl

*OpenUI *CNTonerVolumeAdjustment/Toner Volume Adjustment: PickOne
*DefaultCNTonerVolumeAdjustment: None
*CNTonerVolumeAdjustment None/Printer Default: "<< >>setpagedevice"
*CloseUI: *CNTonerVolumeAdjustment

*OpenUI *CNJobExecMode/Job Execution Mode: PickOne
*DefaultCNJobExecMode: print
*CNJobExecMode print/Print: "<< >>setpagedevice"
*CNJobExecMode secured/Secured Print: "<< >>setpagedevice"
*CNJobExecMode store/Store: "<< >>setpagedevice"
*CloseUI: *CNJobExecMode

*OpenUI *CNColorTonerVolumeAdjustment/Adjust Toner Volume Used for Color Printing: PickOne
*DefaultCNColorTonerVolumeAdjustment: None
*CNColorTonerVolumeAdjustment None/Printer Default: "<< >>setpagedevice"
*CNColorTonerVolumeAdjustment Off/Off: "<< >>setpagedevice"
*CNColorTonerVolumeAdjustment Level1/Level 1: "<< >>setpagedevice"
*CNColorTonerVolumeAdjustment Level2/Level 2: "<< >>setpagedevice"
*CloseUI: *CNColorTonerVolumeAdjustment

*OpenUI *PageSize/Page Size: PickOne
*DefaultPageSize: A4
*PageSize Letter: "<</PageSize[612 792]/ImagingBBox null>>setpagedevice"
*PageSize Legal: "<</PageSize[612 1008]/ImagingBBox null>>setpagedevice"
*PageSize Statement: "<</PageSize[396 612]/ImagingBBox null>>setpagedevice"
*PageSize Executive: "<</PageSize[522 756]/ImagingBBox null>>setpagedevice"
*PageSize A5: "<</PageSize[420 595]/ImagingBBox null>>setpagedevice"
*PageSize B5: "<</PageSize[516 729]/ImagingBBox null>>setpagedevice"
*PageSize A4: "<</PageSize[595 842]/ImagingBBox null>>setpagedevice"
*PageSize Monarch: "<</PageSize[279 540]/ImagingBBox null>>setpagedevice"
*PageSize Com10: "<</PageSize[297 684]/ImagingBBox null>>setpagedevice"
*PageSize dl_envelope: "<</PageSize[312 624]/ImagingBBox null>>setpagedevice"
*PageSize Envelope_C5: "<</PageSize[459 649]/ImagingBBox null>>setpagedevice"
*PageSize Oficio: "<</PageSize[612 901]/ImagingBBox null>>setpagedevice"
*PageSize B_Oficio: "<</PageSize[612 1006]/ImagingBBox null>>setpagedevice"
*PageSize M_Oficio: "<</PageSize[612 967]/ImagingBBox null>>setpagedevice"
*PageSize G_Letter: "<</PageSize[575 757]/ImagingBBox null>>setpagedevice"
*PageSize G_Legal: "<</PageSize[575 935]/ImagingBBox null>>setpagedevice"
*PageSize I_Legal: "<</PageSize[609 978]/ImagingBBox null>>setpagedevice"
*PageSize Foolscap: "<</PageSize[612 936]/ImagingBBox null>>setpagedevice"
*PageSize A_Foolscap: "<</PageSize[584 955]/ImagingBBox null>>setpagedevice"
*PageSize F4A: "<</PageSize[612 972]/ImagingBBox null>>setpagedevice"
*PageSize 16K: "<</PageSize[553 765]/ImagingBBox null>>setpagedevice"
*CloseUI: *PageSize

*OpenUI *PageRegion: PickOne
*DefaultPageRegion: A4
*PageRegion Letter: "<</PageSize[612 792]/ImagingBBox null>>setpagedevice"
*PageRegion Legal: "<</PageSize[612 1008]/ImagingBBox null>>setpagedevice"
*PageRegion Statement: "<</PageSize[396 612]/ImagingBBox null>>setpagedevice"
*PageRegion Executive: "<</PageSize[522 756]/ImagingBBox null>>setpagedevice"
*PageRegion A5: "<</PageSize[420 595]/ImagingBBox null>>setpagedevice"
*PageRegion B5: "<</PageSize[516 729]/ImagingBBox null>>setpagedevice"
*PageRegion A4: "<</PageSize[595 842]/ImagingBBox null>>setpagedevice"
*PageRegion Monarch: "<</PageSize[279 540]/ImagingBBox null>>setpagedevice"
*PageRegion Com10: "<</PageSize[297 684]/ImagingBBox null>>setpagedevice"
*PageRegion dl_envelope: "<</PageSize[312 624]/ImagingBBox null>>setpagedevice"
*PageRegion Envelope_C5: "<</PageSize[459 649]/ImagingBBox null>>setpagedevice"
*PageRegion Oficio: "<</PageSize[612 901]/ImagingBBox null>>setpagedevice"
*PageRegion B_Oficio: "<</PageSize[612 1006]/ImagingBBox null>>setpagedevice"
*PageRegion M_Oficio: "<</PageSize[612 967]/ImagingBBox null>>setpagedevice"
*PageRegion G_Letter: "<</PageSize[575 757]/ImagingBBox null>>setpagedevice"
*PageRegion G_Legal: "<</PageSize[575 935]/ImagingBBox null>>setpagedevice"
*PageRegion I_Legal: "<</PageSize[609 978]/ImagingBBox null>>setpagedevice"
*PageRegion Foolscap: "<</PageSize[612 936]/ImagingBBox null>>setpagedevice"
*PageRegion A_Foolscap: "<</PageSize[584 955]/ImagingBBox null>>setpagedevice"
*PageRegion F4A: "<</PageSize[612 972]/ImagingBBox null>>setpagedevice"
*PageRegion 16K: "<</PageSize[553 765]/ImagingBBox null>>setpagedevice"
*CloseUI: *PageRegion

*DefaultImageableArea: A4
*ImageableArea Letter: "14.173 14.173 597.827 777.827"
*ImageableArea Legal: "14.173 14.173 597.827 993.827"
*ImageableArea Statement: "14.173 14.173 381.827 597.827"
*ImageableArea Executive: "14.173 14.323 507.401 742.827"
*ImageableArea A5: "14.173 13.898 406.771 580.827"
*ImageableArea B5: "14.173 14.67 501.732 714.827"
*ImageableArea A4: "14.173 14.284 581.102 827.827"
*ImageableArea Monarch: "28.346 28.347 250.582 511.654"
*ImageableArea Com10: "28.346 28.347 268.44 655.654"
*ImageableArea dl_envelope: "28.346 28.725 283.464 595.654"
*ImageableArea Envelope_C5: "28.346 28.213 430.866 620.654"
*ImageableArea Oficio: "14.173 14.173 597.827 886.827"
*ImageableArea B_Oficio: "14.173 14.173 597.827 991.827"
*ImageableArea M_Oficio: "14.173 14.173 597.827 952.827"
*ImageableArea G_Letter: "14.173 14.173 560.827 742.827"
*ImageableArea G_Legal: "14.173 14.173 560.827 920.827"
*ImageableArea I_Legal: "14.173 14.173 594.827 963.827"
*ImageableArea Foolscap: "14.160 14.160 597.84 921.840"
*ImageableArea A_Foolscap: "14.173 14.173 569.827 940.827"
*ImageableArea F4A: "14.173 14.173 597.827 957.827"
*ImageableArea 16K: "14.160 14.160 538.840 750.840"

*DefaultPaperDimension: A4
*PaperDimension Letter: "612 792"
*PaperDimension Legal: "612 1008"
*PaperDimension Statement: "396 612"
*PaperDimension Executive: "522 756"
*PaperDimension A5: "420 595"
*PaperDimension B5: "516 729"
*PaperDimension A4: "595 842"
*PaperDimension Monarch: "279 540"
*PaperDimension Com10: "297 684"
*PaperDimension dl_envelope: "312 624"
*PaperDimension Envelope_C5: "459 649"
*PaperDimension Oficio: "612 901"
*PaperDimension B_Oficio: "612 1006"
*PaperDimension M_Oficio: "612 967"
*PaperDimension G_Letter: "575 757"
*PaperDimension G_Legal: "575 935"
*PaperDimension I_Legal: "609 978"
*PaperDimension Foolscap: "612 936"
*PaperDimension A_Foolscap: "584 955"
*PaperDimension F4A: "612 972"
*PaperDimension 16K: "553 765"

*%CNJobAccount: True
*%CNSecuredPrint: True
*%CNMailBox: False
*%CNInputSelect: False
*%CNDisableJobAccountingBW: True
*%CNUserAuthentication: True

*CNOEFLibName: "ufr2filter"
*CNPrinterName: "Canon iR-ADV C256/356 UFR II"
*CNPDLType: "UFR2"
*CNPrtColorSpace: "RGB"
*CN_PdlWrapper_PdlPath: "libcanonufr2"

*CNFeedDirection: "Custom:3"

*CNTblFormat: "2"
*CNTblInputSlot: "3"
*CNTblDuplex: "1"
*CNTblOutputBin: "10"
*CNTblSubstitute: "1"
*CNTblMediaType: "327684"
*CNModelMethod: "1213923628"
*CNModelMethod2: "94"
*CNMaxBoxNum: "99"
*CNMaxDocStr: "32"
*CNMaxCopies: "9999"
*CNMaxGutter: "50"
*CNShiftStartPrintPosType: "1"
*CNExecuteMethod: "0"
*CNUFR2ModelMethod: "36864"
*CN_PDL_CTN: "1"
*CNMaxSendDataSize: "0xFFFE"
*CNRegistSocket: "1"
*%CNBookletOffset: "1"
*%CNDLCustomProfile: "1"
*%CNUIValue: *CNFinDetails(True):False
*%CNUIValue: *EnableCNOffsetNum(True):False
*%CNUIValue: *CNUIOffsetMax(9999):False
*%CNUIValue: *CNShiftPositionMin(-50):False
*%CNUIValue: *CNShiftPositionMax(50):False

*UIConstraints: *CNEnableTrustPrint False *CNTrustPrint True

*UIConstraints: *MediaType OHP *CNCopySetNumbering True

*UIConstraints: *Collate StapleGroup *CNCopySetNumbering True
*UIConstraints: *Collate Group *CNCopySetNumbering True

*UIConstraints: *Duplex DuplexTumble *BindEdge Left
*UIConstraints: *Duplex DuplexNoTumble *BindEdge Top

*UIConstraints: *BindEdge Top *StapleLocation BottomLeft
*UIConstraints: *BindEdge Left *StapleLocation TopRight

*UIConstraints: *CNCopySetNumbering True *CNTrustPrint True

*UIConstraints: *CNJobExecMode store *Collate StapleGroup

*UIConstraints: *Collate StapleCollate *CNOutputPartition offset
*UIConstraints: *Collate StapleGroup *CNOutputPartition offset

*UIConstraints: *CNFinisher None *CNOutputPartition offset

*%CNUIChangeDefault: *CNFinisher(STFINZ1):*CNOutputPartition(offset)

*UIConstraints: *CNFinisher None *Collate StapleCollate
*UIConstraints: *CNFinisher None *Collate StapleGroup

*UIConstraints: *PageSize Monarch *Duplex DuplexTumble
*UIConstraints: *PageSize Monarch *Duplex DuplexNoTumble
*UIConstraints: *PageSize Com10 *Duplex DuplexTumble
*UIConstraints: *PageSize Com10 *Duplex DuplexNoTumble
*UIConstraints: *PageSize dl_envelope *Duplex DuplexTumble
*UIConstraints: *PageSize dl_envelope *Duplex DuplexNoTumble
*UIConstraints: *PageSize Envelope_C5 *Duplex DuplexTumble
*UIConstraints: *PageSize Envelope_C5 *Duplex DuplexNoTumble

*UIConstraints: *PageSize Monarch *CNOutputPartition offset
*UIConstraints: *PageSize Com10 *CNOutputPartition offset
*UIConstraints: *PageSize dl_envelope *CNOutputPartition offset
*UIConstraints: *PageSize Envelope_C5 *CNOutputPartition offset

*UIConstraints: *PageSize Statement *Collate StapleCollate
*UIConstraints: *PageSize Statement *Collate StapleGroup
*UIConstraints: *PageSize A5 *Collate StapleCollate
*UIConstraints: *PageSize A5 *Collate StapleGroup
*UIConstraints: *PageSize Monarch *Collate StapleCollate
*UIConstraints: *PageSize Monarch *Collate StapleGroup
*UIConstraints: *PageSize Com10 *Collate StapleCollate
*UIConstraints: *PageSize Com10 *Collate StapleGroup
*UIConstraints: *PageSize dl_envelope *Collate StapleCollate
*UIConstraints: *PageSize dl_envelope *Collate StapleGroup
*UIConstraints: *PageSize Envelope_C5 *Collate StapleCollate
*UIConstraints: *PageSize Envelope_C5 *Collate StapleGroup

*UIConstraints: *MediaType HEAVY4 *Duplex DuplexTumble
*UIConstraints: *MediaType HEAVY4 *Duplex DuplexNoTumble
*UIConstraints: *MediaType HEAVY5 *Duplex DuplexTumble
*UIConstraints: *MediaType HEAVY5 *Duplex DuplexNoTumble
*UIConstraints: *MediaType OHP *Duplex DuplexTumble
*UIConstraints: *MediaType OHP *Duplex DuplexNoTumble
*UIConstraints: *MediaType LABELS *Duplex DuplexTumble
*UIConstraints: *MediaType LABELS *Duplex DuplexNoTumble
*UIConstraints: *MediaType ENVELOPE *Duplex DuplexTumble
*UIConstraints: *MediaType ENVELOPE *Duplex DuplexNoTumble

*UIConstraints: *MediaType OHP *CNOutputPartition offset
*UIConstraints: *MediaType LABELS *CNOutputPartition offset
*UIConstraints: *MediaType ENVELOPE *CNOutputPartition offset

*UIConstraints: *MediaType OHP *Collate StapleCollate
*UIConstraints: *MediaType OHP *Collate StapleGroup
*UIConstraints: *MediaType LABELS *Collate StapleCollate
*UIConstraints: *MediaType LABELS *Collate StapleGroup
*UIConstraints: *MediaType ENVELOPE *Collate StapleCollate
*UIConstraints: *MediaType ENVELOPE *Collate StapleGroup

*UIConstraints: *MediaType HEAVY4 *StapleLocation BottomLeft
*UIConstraints: *MediaType HEAVY4 *StapleLocation TopRight
*UIConstraints: *MediaType HEAVY5 *StapleLocation BottomLeft
*UIConstraints: *MediaType HEAVY5 *StapleLocation TopRight

*DefaultFont: Courier
*Font AvantGarde-Book: Standard "(001.006S)" Standard ROM
*Font AvantGarde-BookOblique: Standard "(001.006S)" Standard ROM
*Font AvantGarde-Demi: Standard "(001.007S)" Standard ROM
*Font AvantGarde-DemiOblique: Standard "(001.007S)" Standard ROM
*Font Bookman-Demi: Standard "(001.004S)" Standard ROM
*Font Bookman-DemiItalic: Standard "(001.004S)" Standard ROM
*Font Bookman-Light: Standard "(001.004S)" Standard ROM
*Font Bookman-LightItalic: Standard "(001.004S)" Standard ROM
*Font Courier: Standard "(002.004S)" Standard ROM
*Font Courier-Bold: Standard "(002.004S)" Standard ROM
*Font Courier-BoldOblique: Standard "(002.004S)" Standard ROM
*Font Courier-Oblique: Standard "(002.004S)" Standard ROM
*Font Helvetica: Standard "(001.006S)" Standard ROM
*Font Helvetica-Bold: Standard "(001.007S)" Standard ROM
*Font Helvetica-BoldOblique: Standard "(001.007S)" Standard ROM
*Font Helvetica-Narrow: Standard "(001.006S)" Standard ROM
*Font Helvetica-Narrow-Bold: Standard "(001.007S)" Standard ROM
*Font Helvetica-Narrow-BoldOblique: Standard "(001.007S)" Standard ROM
*Font Helvetica-Narrow-Oblique: Standard "(001.006S)" Standard ROM
*Font Helvetica-Oblique: Standard "(001.006S)" Standard ROM
*Font NewCenturySchlbk-Bold: Standard "(001.009S)" Standard ROM
*Font NewCenturySchlbk-BoldItalic: Standard "(001.007S)" Standard ROM
*Font NewCenturySchlbk-Italic: Standard "(001.006S)" Standard ROM
*Font NewCenturySchlbk-Roman: Standard "(001.007S)" Standard ROM
*Font Palatino-Bold: Standard "(001.005S)" Standard ROM
*Font Palatino-BoldItalic: Standard "(001.005S)" Standard ROM
*Font Palatino-Italic: Standard "(001.005S)" Standard ROM
*Font Palatino-Roman: Standard "(001.005S)" Standard ROM
*Font Symbol: Special "(001.007S)" Special ROM
*Font Times-Bold: Standard "(001.007S)" Standard ROM
*Font Times-BoldItalic: Standard "(001.009S)" Standard ROM
*Font Times-Italic: Standard "(001.007S)" Standard ROM
*Font Times-Roman: Standard "(001.007S)" Standard ROM
*Font ZapfChancery-MediumItalic: Standard "(001.007S)" Standard ROM
*Font ZapfDingbats: Special "(001.004S)" Standard ROM

*%
*% End of PPD file.
*%
